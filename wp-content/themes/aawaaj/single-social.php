<?php get_header(); ?>
<div class="thumbnail-div">
	<img src="<?php the_post_thumbnail_url();?>">
</div>

<div class="container-div">
	<div class="views-div">
		<a href="#">
			<i class="fas fa-share-alt"></i>
			<small>Share</small>
		</a>
		<i class="fas fa-eye"></i>
		<small><?php echo do_shortcode('[post-views]') ?></small>
	</div>

	<div class="row single-row" id="single-row">
		<div class="col-lg-12  type-div">
			<div class="type">
				<p>Social</p>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-12 content-div">
				<h1 class="single-heading"><?php echo(get_the_title()); ?></h1>
				<small class="single-small"><?php echo(get_field('date')); ?></small>
				<br>
				<br>
				<br>
				<?php echo(get_field('top_paragraph')); ?>
				<br>
				<div class="row">
					<div class="col-lg-12 single-image">
						<img src="<?php echo(get_field('image')) ?>">
					</div>
				</div>
				<br>
				<br>
				<?php echo(get_field('middle_paragraph')); ?>
				<br>
				<blockquote class="blockquote"><p><?php echo(get_field('highlight')); ?></p></blockquote>
				<br>
				<?php echo(get_field('bottom_paragraph')) ?>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-12 social-media">
						<small class="share"><b>Share</b></small>
						<a href="#"><i class="fab fa-twitter" style="font-size: 50px;color: #33ccff; padding-right:15px"></i></a>
						<a href="#"><i class="fab fa-facebook-f" style="font-size:50px;color:#3c5a99;padding-right: 15px;"></i></a>
						<a href="#"><i class="fab fa-instagram" style="font-size: 50px;color: #c537a0"></i></a>
					</div>
					
						<div class="col-lg-6 col-md-6 col-sm-12 col-12 audio-player">
							<?php
								$file = get_field('audio_file');
								if( $file ): ?>
    							<audio
							        controls
							        src="<?php echo $file; ?>">
							            Your browser does not support the
							            <code>audio</code> element.
							    </audio>
							<?php endif; ?>
							
						</div>
				</div>
			</div>
		</div>
		<div class="row next-previous">
			<?php 
				$prev_post = get_previous_post(); 
				$prevThumb = get_the_post_thumbnail_url( $prev_post, 'thumbnail-size' );
				$prev_link=get_permalink($prev_post);
				$prev_title=get_the_title($prev_post);
			?>
			
			<div class="col-lg-6 col-md-6 col-sm-12 col-12 previous">
				<a href="<?php echo $prev_link ?>">
					<div class="col-lg-12">
						<div class="row">
							<div class="col-lg-4 previous-image">
								<img src="<?php echo $prevThumb ?>">
							</div>
							<div class="col-lg-8">
								<div class="type">
									<p>Previous</p>
								</div>
								<div class="paragraph-previous">
									<p><?php echo($prev_title) ?></p>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
			

			<?php 
				$next_post=get_next_post();
				$nextThumb=get_the_post_thumbnail_url( $next_post, 'thumbnail-size' );
				$next_link=get_permalink($next_post);
				$next_title=get_the_title($next_post);
			?>

			<div class="col-lg-6 col-md-6 col-sm-12 col-12">
				<a href="<?php echo $next_link ?>">
					<div class="col-lg-12 next">
						<div class="row">
							<div class="col-lg-8 next-text">
								<div class="type" style="height: 40px;">
									<p>Next</p>
								</div>
								<div class="paragraph-next">
									<p><?php echo($next_title) ?></p>
								</div>
							</div>
							<div class="col-lg-4 next-image">
								<img src="<?php echo $nextThumb; ?>">
							</div>
						</div>
					</div>
				</a>
				<div class="col-md-12 single-up-arrow">
					<a href="#single-row" id="scroll"><i class="fas fa-arrow-circle-up" style="font-size: 50px;color: #007bff;padding-right: 10px;"></i></a>
				</div>
			</div>
				
		</div>
		<div class="row related-row">
			<div class="col-md-12 related-title">
				<h3>Related Posts</h3>
			</div>
			<?php
				$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3,'post_type' => 'social', 'post__not_in' => array($post->ID) ) );
				if( $related ) foreach( $related as $post ) {
					setup_postdata($post); ?>
					<div class="col-lg-4 col-md-4 col-sm-12 col-12 related">
						<a href="<?php the_permalink(); ?>">
							<div class="col-lg-12 related-container">
								<img src="<?php echo the_post_thumbnail_url() ?>">
								<div class="wrapper">
									<div class="text">
									<p><?php the_title(); ?></p>
								</div>
								</div>
							</div>
						</a>
					</div>
			<?php }
			wp_reset_postdata(); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

