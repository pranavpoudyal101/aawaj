<?php 
	function load_css(){
		wp_register_style('bootstrap',get_template_directory_uri() . '/css/bootstrap.min.css',array(),false,'all');
		wp_enqueue_style('bootstrap');

		wp_register_style('main',get_template_directory_uri() . '/css/main.css',array(),false,'all');
		wp_enqueue_style('main');
	}

	add_action('wp_enqueue_scripts','load_css');

	//load scripts
	function load_js(){
		wp_enqueue_script('jquery');
		wp_register_script('bootstrap',get_template_directory_uri() . '/js/bootstrap.min.js','jquery',false,true);
		wp_enqueue_script('bootstrap');
		wp_register_script('custom',get_template_directory_uri() . '/custom.js',array(),1,1,1);
		wp_enqueue_script('custom');
	}

	add_action('wp_enqueue_scripts','load_js');

	add_theme_support('post-thumbnails');
	add_theme_support('menus');
	// add_theme_support('widgets');

	register_nav_menus(
		array(
			'top-menu'=>'Top Menu Location',
		)
	);

	function social_news(){
		$args=array(
			'labels'=>array(
				'name'=>'Social News',
				'singular'=>'Social'
			),
			'hierarchical'=>false,
			'public'=>true,
			'archive'=>true,
			'menu_icon'=>'dashicons-images-alt2',
			'supports'=>array('thumbnail','title','custom-fields'),
			// 'rewrite'=>array('slug'=>'cars'),
		);
		register_post_type('social',$args);
	}

	add_action('init','social_news');

	function political_news(){
		$args=array(
			'labels'=>array(
				'name'=>'Political News',
				'singular'=>'Political'
			),
			'hierarchical'=>false,
			'public'=>true,
			'archive'=>true,
			'menu_icon'=>'dashicons-images-alt2',
			'supports'=>array('thumbnail','title','custom-fields'),
		);
		register_post_type('political',$args);
	}

	add_action('init','political_news');

	function aawaaj_video(){
		$args=array(
			'labels'=>array(
				'name'=>'Aawaaj Videos',
				'singular'=>'Aawaaj Video'
			),
			'hierarchical'=>false,
			'public'=>true,
			'archive'=>true,
			'menu_icon'=>'dashicons-images-alt2',
			'supports'=>array('title','thumbnail'),
		);
		register_post_type('video',$args);
	}

	add_action('init','aawaaj_video','custom-fields');

	


	function weeks(){
		$args=array(
			'labels'=>array(
				'name'=>'Democracy Weeks',
				'singular'=>'Week'
			),
			'hierarchical'=>true,
			'public'=>true,
			'archive'=>true,
			'menu_icon'=>'dashicons-images-alt2',
			'supports'=>array('title','thumbnail','custom-fields'),
		);
		register_post_type('weeks',$args);
	}

	add_action('init','weeks');

	function democracy_taxonomy()
	{
		$args=array(
			'labels'=>array(
				'name'=>'Activists',
				'singular_name'=>'Activist',
			),
			'public'=>true,
			'hierarchical'=>true,
		);
		register_taxonomy('activist',array('weeks'),$args);
	}
	add_action('init','democracy_taxonomy');

	add_filter( 'redirect_canonical', 'custom_disable_redirect_canonical' );
	function custom_disable_redirect_canonical( $redirect_url ) {
    	if ( is_paged() && is_singular() ) $redirect_url = false; 
    	return $redirect_url; 
	}
?>