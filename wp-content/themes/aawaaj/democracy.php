<div class="thumbnail-div">
  <img src="<?php the_field( 'image', 'activist_' . $category );?>">
  <div class="democracy-title">
    <p><?php the_field('caption_1','activist_' . $category); ?></p>
    <p><?php the_field('caption_2','activist_' . $category); ?><p>
  </div>
</div>

<div class="container-div">
  <div class="views-div">
    <a href="#">
      <i class="fas fa-share-alt"></i>
      <small>Share</small>
    </a>
    <i class="fas fa-eye"></i>
    <small><?php echo do_shortcode('[post-views]') ?></small>
  </div>
  <div class="row single-row" id="democracy-row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-12 week-div">
      <div class="left-next-container">
        <a href="#">
          <div class="previous-button">
            <i class="fas fa-chevron-left" style="font-size: 25px;color: #fff;"></i>
          </div>
        </a>
        <?php

          //get the posts in ascending order
          $posts =new WP_Query(
            array( 
              'post_type' => 'weeks',
              'orderby' => 'date',
              'order'   => 'ASC',
              // 'tax_query' => array(
              //   array(
              //       'taxonomy' => 'activist',
              //       // 'field' => 'term_id',
              //       // 'terms' => $category,
              //   )
              // )
            )
          );

          if($posts->have_posts()): 
                while($posts->have_posts()): $posts->the_post();
        ?>
        <a href="<?php echo(get_the_permalink()) ?>">
          <div class="week">

            <small><?php echo get_the_title() ?></small>
          </div>
        </a>
        <?php endwhile;endif;wp_reset_query(); ?>
      </div>
      <div class="right-next-container">
        <a href="#">
          <div class="next-button">
            <i class="fas fa-chevron-right" style="font-size: 25px;color: #fff;"></i>
          </div>
        </a>
      </div>
  </div>
  <?php
    $latest = new WP_Query(
      array( 
        'post_type' => 'weeks',
        'posts_per_page'=>1,
        'orderby' => 'date',
        'order'   => 'DESC',
        'tax_query' => array(
          array(
              'taxonomy' => 'activist',
              'field' => 'term_id',
              'terms' => $category,
          )
        )
      )
    );
    if($latest->have_posts()): 
                while($latest->have_posts()): $latest->the_post();
  ?>
  <div class="row democracy-content-row">
    <div class="col-lg-12 col-md-12 democracy-content">
      <h1><?php echo(get_field('main_title')); ?></h1>


      <div class="col-lg-12 col-md-12 slider-div">
          <img src="<?php bloginfo('template_directory');?>/images/ganesh.jpg">
      </div>


      <h4><?php echo(get_field('title_one')); ?></h4>


      <?php echo(get_field('title_one_content')); ?>


      <blockquote class="blockquote">
        <p>
          <?php echo(get_field('blockquote_one_for_title_one_content')); ?>
        </p>
      </blockquote>


      <div class="lightbox">
          <img src="<?php bloginfo('template_directory');?>/images/ganesh.jpg">
      </div>



      <blockquote class="blockquote">
        <p>
          <?php echo(get_field('blockquote_two_for_title_one_content')); ?>
        </p>
      </blockquote>


      <?php echo get_field('title_one_content_two'); ?>


      <div class="lightbox">
          <img src="<?php bloginfo('template_directory');?>/images/ganesh.jpg">
      </div>


      <h4><?php echo(get_field('title_two')); ?></h4>

      <?php echo get_field('content_two'); ?>
      </div>
      <div class="row democracy-social-media">
          <div class="col-lg-6 col-md-6 col-sm-12 col-12 social-media">
            <small class="share"><b>Share</b></small>
            <a href="#"><i class="fab fa-twitter" style="font-size: 50px;color: #33ccff; padding-right:15px"></i></a>
            <a href="#"><i class="fab fa-facebook-f" style="font-size:50px;color:#3c5a99;padding-right: 15px;"></i></a>
            <a href="#"><i class="fab fa-instagram" style="font-size: 50px;color: #c537a0"></i></a>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-12 audio-player">
            <?php
                $file = get_field('audio_file');
                if( $file ): ?>
                  <audio
                      controls
                      src="<?php echo $file; ?>">
                          Your browser does not support the
                          <code>audio</code> element.
                  </audio>
              <?php endif; ?>
          </div>
        </div>
        <div class="row next-previous">
      <?php 
        $prev_post = get_previous_post(); 
        $prevThumb = get_the_post_thumbnail_url( $prev_post, 'thumbnail-size' );
        $prev_link=get_permalink($prev_post);
        $prev_title=get_the_title($prev_post);
      ?>
      
      <div class="col-lg-6 col-md-6 col-sm-12 col-12 previous">
        <a href="<?php echo $prev_link ?>">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-lg-4 previous-image">
                <img src="<?php echo $prevThumb ?>">
              </div>
              <div class="col-lg-8">
                <div class="type">
                  <p>Previous</p>
                </div>
                <div class="paragraph-previous">
                  <p><?php echo($prev_title) ?></p>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      

      <?php 
        $next_post=get_next_post();
        $nextThumb=get_the_post_thumbnail_url( $next_post, 'thumbnail-size' );
        $next_link=get_permalink($next_post);
        $next_title=get_the_title($next_post);
      ?>

      <div class="col-lg-6 col-md-6 col-sm-12 col-12">
        <a href="<?php echo $next_link ?>">
          <div class="col-lg-12 next">
            <div class="row">
              <div class="col-lg-8 next-text">
                <div class="type" style="height: 40px;">
                  <p>Next</p>
                </div>
                <div class="paragraph-next">
                  <p><?php echo($next_title) ?></p>
                </div>
              </div>
              <div class="col-lg-4 next-image">
                <img src="<?php echo $nextThumb; ?>">
              </div>
            </div>
          </div>
        </a>
        <div class="col-md-12 single-up-arrow">
          <a href="#democracy-row" id="scroll"><i class="fas fa-arrow-circle-up" style="font-size: 50px;color: #007bff;padding-right: 10px;"></i></a>
        </div>
      </div>
        
    </div>
    </div>
  </div>
<?php endwhile;endif;wp_reset_query(); ?>
  
</div>










<?php get_header() ?>
  <?php 
    $terms=get_the_terms( $post, 'activist' ); 
    $category=($terms[0]->term_id);
  ?>


  <div class="thumbnail-div">
      <img src="<?php the_field( 'image', 'activist_' . $category );?>">
      <div class="democracy-title">
        <p><?php the_field('caption_1','activist_' . $category); ?></p>
        <p><?php the_field('caption_2','activist_' . $category); ?><p>
      </div>
  </div>

  <div class="container-div">
    <div class="views-div">
      <a href="#">
        <i class="fas fa-share-alt"></i>
        <small>Share</small>
      </a>
      <i class="fas fa-eye"></i>
      <small><?php echo do_shortcode('[post-views]') ?></small>
    </div>
    <div class="row single-row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-12 week-div">
        <div class="left-next-container">
          <a href="#">
            <div class="previous-button">
              <i class="fas fa-chevron-left" style="font-size: 25px;color: #fff;"></i>
            </div>
          </a>
          <?php

            //get the posts in ascending order
            $posts =new WP_Query(
              array( 
                'post_type' => 'weeks',
                'orderby' => 'date',
                'order'   => 'ASC',
                'tax_query' => array(
                  array(
                      'taxonomy' => 'activist',
                      'field' => 'term_id',
                      'terms' => $category,
                  )
                )
              )
            );

            if($posts->have_posts()): 
                  while($posts->have_posts()): $posts->the_post();
          ?>
          <a href="<?php echo(get_the_permalink()) ?>">
            <div class="week">

              <small><?php echo get_the_title() ?></small>
            </div>
          </a>
          <?php endwhile;endif;wp_reset_query(); ?>
        </div>
        <div class="right-next-container">
          <a href="#">
            <div class="next-button">
              <i class="fas fa-chevron-right" style="font-size: 25px;color: #fff;"></i>
            </div>
          </a>
        </div>
    </div>
    <div class="row democracy-content-row" id="democracy-row">
    <div class="col-lg-12 col-md-12 democracy-content">
      <h1><?php echo(get_field('main_title')); ?></h1>


      <div class="col-lg-12 col-md-12 slider-div">
          <img src="<?php bloginfo('template_directory');?>/images/ganesh.jpg">
      </div>


      <h4><?php echo(get_field('title_one')); ?></h4>


      <?php echo(get_field('title_one_content')); ?>


      <blockquote class="blockquote">
        <p>
          <?php echo(get_field('blockquote_one_for_title_one_content')); ?>
        </p>
      </blockquote>


      <div class="lightbox">
          <img src="<?php bloginfo('template_directory');?>/images/ganesh.jpg">
      </div>



      <blockquote class="blockquote">
        <p>
          <?php echo(get_field('blockquote_two_for_title_one_content')); ?>
        </p>
      </blockquote>


      <?php echo get_field('title_one_content_two'); ?>


      <div class="lightbox">
          <img src="<?php bloginfo('template_directory');?>/images/ganesh.jpg">
      </div>


      <h4><?php echo(get_field('title_two')); ?></h4>

      <?php echo get_field('content_two'); ?>
      </div>
      <div class="row democracy-social-media">
          <div class="col-lg-6 col-md-6 col-sm-12 col-12 social-media">
            <small class="share"><b>Share</b></small>
            <a href="#"><i class="fab fa-twitter" style="font-size: 50px;color: #33ccff; padding-right:15px"></i></a>
            <a href="#"><i class="fab fa-facebook-f" style="font-size:50px;color:#3c5a99;padding-right: 15px;"></i></a>
            <a href="#"><i class="fab fa-instagram" style="font-size: 50px;color: #c537a0"></i></a>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-12 audio-player">
            <?php
                $file = get_field('audio_file');
                if( $file ): ?>
                  <audio
                      controls
                      src="<?php echo $file; ?>">
                          Your browser does not support the
                          <code>audio</code> element.
                  </audio>
              <?php endif; ?>
          </div>
        </div>
    </div>
    <div class="row next-previous">
      <?php 
        $prev_post = get_previous_post(); 
        $prevThumb = get_the_post_thumbnail_url( $prev_post, 'thumbnail-size' );
        $prev_link=get_permalink($prev_post);
        $prev_title=get_the_title($prev_post);
      ?>
      
      <div class="col-lg-6 col-md-6 col-sm-12 col-12 previous">
        <a href="<?php echo $prev_link ?>">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-lg-4 previous-image">
                <img src="<?php echo $prevThumb ?>">
              </div>
              <div class="col-lg-8">
                <div class="type">
                  <p>Previous</p>
                </div>
                <div class="paragraph-previous">
                  <p><?php echo($prev_title) ?></p>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      

      <?php 
        $next_post=get_next_post();
        $nextThumb=get_the_post_thumbnail_url( $next_post, 'thumbnail-size' );
        $next_link=get_permalink($next_post);
        $next_title=get_the_title($next_post);
      ?>

      <div class="col-lg-6 col-md-6 col-sm-12 col-12">
        <a href="<?php echo $next_link ?>">
          <div class="col-lg-12 next">
            <div class="row">
              <div class="col-lg-8 next-text">
                <div class="type" style="height: 40px;">
                  <p>Next</p>
                </div>
                <div class="paragraph-next">
                  <p><?php echo($next_title) ?></p>
                </div>
              </div>
              <div class="col-lg-4 next-image">
                <img src="<?php echo $nextThumb; ?>">
              </div>
            </div>
          </div>
        </a>
        <div class="col-md-12 single-up-arrow">
          <a href="#democracy-row" id="scroll"><i class="fas fa-arrow-circle-up" style="font-size: 50px;color: #007bff;padding-right: 10px;"></i></a>
        </div>
      </div>
        
    </div>
  </div>
</div>


<?php get_footer() ?>