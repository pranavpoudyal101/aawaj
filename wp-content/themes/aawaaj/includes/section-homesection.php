<div class="outer">
	<div class="row row-margin">
		<div class="col-lg-6 social">
			<div class="col-lg-12 news">
				<div class="col-lg-4 col-md-4 col-sm-6 col-8 category-heading">
					<h2>Political</h2>
				</div>
			</div>
			<?php 
                $query= new WP_Query(
                array(
                    'post_type'         => 'political', 
                    'posts_per_page'    => 3, 
                    'meta_key'=> 'date',
                    'order'=> 'DESC'
                    )); 
                if($query->have_posts()): 
                while($query->have_posts()): $query->the_post();
                ?>
            <a href="<?php the_permalink(); ?>">
				<div class="col-lg-12 news">
					<img src="<?php the_post_thumbnail_url(); ?>">
					<div class="wrapper">
						<div class="page-text">
							<p><?php echo(get_the_title()) ?></p>
							<small class="date"><?php echo(get_field('date')) ?></small>
						</div>	
					</div>
				</div>
			</a>
			<?php endwhile;endif; wp_reset_query();?>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-12 political">
			<div class="col-lg-12 news">
				<div class="col-lg-12 news">
					<div class="col-lg-4 col-md-4 col-sm-6 col-8 category-heading">
						<h2>Social</h2>
					</div>
				</div>
			</div>
			<?php 
                $query= new WP_Query(
                array(
                    'post_type'         => 'social', 
                    'posts_per_page'    => 3,
                    'meta_key'=> 'date',
                    'order'=> 'DESC'
                )
               	); 
                if($query->have_posts()): 
                while($query->have_posts()): $query->the_post();
                ?>
            <a href="<?php the_permalink(); ?>">
				<div class="col-lg-12 col-md-12 col-sm-12 news">
					<img src="<?php the_post_thumbnail_url(); ?>">
					<div class="wrapper">
						<div class="page-text">
							<p><?php echo(get_the_title()) ?></p>
							<small class="date"><?php echo(get_field('date')) ?></small>
						</div>	
					</div>
				</div>
			</a>
			<?php endwhile;endif; wp_reset_query();?>
		</div>
	</div>
	<div class="row video">
		<div class="col-lg-12 col-md-12 col-sm-12 video-container">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading-container">
				<div class="col-lg-4 col-md-4 col-sm-6 col-8 category-heading">
					<h2>Awaaj Videos</h2>
				</div>
			</div>
			<div class="row">
				<?php 
            		$video= new WP_Query(
            		array(
                		'post_type' => 'video', 
                		'meta_query' => array( 'relation' => 'AND', array(
    									'meta_key'     => 'featured_video',
    									'meta_value'   => true,
    									'meta_compare' => '='
								) 
                			)
            			)
           			);
           			if($video->have_posts()): 
                	while($video->have_posts()): $video->the_post();
                		if(get_field('featured_video')):
                ?>
							<div class="col-lg-8 col-md-12 col-sm-12 col-12 featured-video">
								<?php echo (get_field('link')); ?>
							</div>
						<?php endif ?>
					<?php endwhile;endif; wp_reset_query();?>

				<div class="col-lg-4 col-md-12 col-sm-12 col-12 other-videos">
					<?php 
            		$video= new WP_Query(
            		array(
                		'post_type' => 'video', 
            			)
           			);
           			if($video->have_posts()): 
                	while($video->have_posts()): $video->the_post();
                		if(!get_field('featured_video')):
                ?>	
					<div class="col-lg-12 sidebar-video">
						<?php 
							echo (get_field('link'));
						?>
					</div>
					<?php endif; ?>
				<?php endwhile;endif;wp_reset_query(); ?>
				</div>

			</div>
		</div>
	</div>

	<div class="row map">
		<div class="col-lg-12 col-12 map-container">
			<img src="<?php bloginfo('template_directory');?>/images/world map.jpg">
		</div>
	</div>
</div>