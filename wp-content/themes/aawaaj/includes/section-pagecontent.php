<div class="outer">
	<div class="row row-margin-page" id="row">
		<?php 
				$paged = (get_query_var('paged')) ? get_query_var('paged') :1;
                $query= new WP_Query(
                array(
                    'post_type'         => 'social', 
                    'posts_per_page'    => 8, 
                    'meta_key'=> 'date',
                    'order'=> 'DESC',
                    'paged'=>$paged,
                    )); 
                if($query->have_posts()): 
                while($query->have_posts()): $query->the_post();
                ?>
					<div class="col-lg-6 news">
						<a href="<?php the_permalink(); ?>">
							<img src="<?php the_post_thumbnail_url();?>">
							<div class="wrapper">
								<div class="page-text">
									<p><?php echo(get_the_title()); ?></p>
									<small class="date"><?php echo(get_field('date')) ?></small>
								</div>	
							</div>
						</a>
					</div>
				
		

		<?php endwhile;?>
		<?php 
			$total_pages = $query->max_num_pages;

    		if ($total_pages > 1){

        	$current_page = max(1, get_query_var('paged'));

	        echo paginate_links(array(
	            'base' => get_pagenum_link(1) . '%_%',
	            'format' => '/page/%#%',
	            'current' => $current_page,
	            'total' => $total_pages,
	            'prev_text'    => __('« prev'),
	            'next_text'    => __('next »'),
	        ));
   			}    
		?>
		<?php endif; wp_reset_query(); ?>
		<div class="col-md-12 up-arrow">
			<a href="#row" id="scroll"><i class="fas fa-arrow-circle-up" style="font-size: 50px;color: #007bff;"></i></a>
		</div>
	</div>
</div>	