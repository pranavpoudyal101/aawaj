<div class="outer">
	<div class="row row-margin-page" id="row">
		<?php 
                $query= new WP_Query(
                array(
                    'post_type'         => 'political', 
                    'posts_per_page'    => 8, 
                    'meta_key'=> 'date',
                    'meta_key'=> 'date',
                    'order'=> 'DESC',
                    )); 
                if($query->have_posts()): 
                while($query->have_posts()): $query->the_post();
                ?>
				<div class="col-lg-6 news">
					<a href="<?php the_permalink(); ?>">
						<img src="<?php the_post_thumbnail_url();?>">
						<div class="wrapper">
							<div class="page-text">
								<p><?php echo(get_the_title()); ?></p>
								<small class="date"><?php echo(get_field('date')) ?></small>
							</div>	
						</div>
					</a>
				</div>
				
		

		<?php endwhile;endif; wp_reset_query();?>
		<div class="col-md-12 up-arrow">
			<a href="#row" id="scroll"><i class="fas fa-arrow-circle-up" style="font-size: 50px;color: #007bff;"></i></a>
		</div>
	</div>
</div>


