<div class="content">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-12 slider">
			<?php echo do_shortcode('[recent_post_carousel post_type="political" limit="5" media_size="medium" dots="false" show_author="false" show_read_more="false"]'); ?> 
		</div>
	</div>
</div>