<?php get_header(); ?>
	<div class="content clearfix">
		<div class="row">
			<div class="col-lg-4 col-md-12 col-sm-12 col-12 main-banner-image">
				<?php 
            		$main= new WP_Query(
            		array(
                		'post_type' => array('social','political'), 
            			)
           			);
           			if($main->have_posts()): 
                	while($main->have_posts()): $main->the_post();
                		if(get_field('main_banner')):
                ?>
                <a href="<?php the_permalink(); ?>">
					<img src="<?php echo the_post_thumbnail_url();?>">
					<div class="wrapper2">
						<div class="text">
							<p><?php echo get_the_title(); ?></p>
						</div>
					</div>
				</a>
				<?php endif; ?>
				<?php endwhile;endif;wp_reset_query(); ?>
			</div>
			<div class="col-lg-8 col-md-12 col-sm-12 col-12 banner-images">
				<div class="row">
					<?php 
	            		$side= new WP_Query(
	            		array(
	                		'post_type' => array('social','political'), 
	            			)
	           			);
	           			if($side->have_posts()): 
	                	while($side->have_posts()): $side->the_post();
	                		if(get_field('side_banner')):
	                ?>
					<div class="col-lg-6 col-md-6 col-sm-12 col-12 banner-image">
						<a href="<?php the_permalink() ?>">
							<img src="<?php echo the_post_thumbnail_url(); ?>">
							<div class="wrapper">
								<div class="text">
									<p><?php echo get_the_title(); ?></p>
								</div>	
							</div>
						</a>
					</div>
					<?php endif; ?>
					<?php endwhile;endif;wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part('includes/section','homesection'); ?>
<?php get_footer(); ?>