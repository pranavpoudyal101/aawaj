<!DOCTYPE html>
<html>
<head>
	<title>Aawaaj News</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">

    <?php wp_head(); ?>
</head>
<body>
	<header>
		<div class="header-div col-xl-12 col-lg-12 col-sm-12 col-xs-12">
			<div class="header-top clearfix">
				<div class="header-logo"><a href="<?php echo(site_url('/')); ?>"><img src="<?php bloginfo('template_directory');?>/images/logo_header.png"></a></div>
				<div class="header-date"><small><?php echo(date("l, jS F")); ?></small></div>
			</div>
			<div id="header-bottom" class="header-bottom">
				<?php 
					wp_nav_menu(
						array(
							'theme_location'=>'top-menu',
						)
					)
				?>
			</div>

		</div>
	</header>


