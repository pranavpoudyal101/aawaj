<footer>
	<div class="row footer">
		<div class="col-lg-4 col-md-12 col-sm-12 col-12 footerdiv">
			<img src="<?php bloginfo('template_directory');?>/images/logo_header.png"><br>
			<small>&copy; 2019 By Nepali Aawaaz Media Pvt.Ltd </small><br>
			<small> Reg.No:2069/070/071</small>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-12 footerdiv2">
			<div>
				<small>About Us | Contact | Privacy Policy</small>
			</div>
			<div>
				<i class="fab fa-twitter" style="font-size: 50px; padding-right: 25px;"></i>
				<i class="fab fa-facebook-f" style="font-size: 50px; padding-right: 25px;"></i>
				<i class="fab fa-instagram" style="font-size: 50px;"></i>
			</div>
			<div>
			</div>
		</div>
		<div class="col-lg-4 col-md-12 col-sm-12 col-12 footerdiv">
			<p><b>Get In Touch/Share Your Story</b></p>
			<small><i class="far fa-envelope-open" style="color: #cb221f"></i> aawaazmedia@gmail.com</small><br>
			<small><i class="fas fa-phone-volume" style="color: #cb221f"></i> 9859726666</small><br>
			<small><i class="fas fa-map-pin" style="color:#cb221f"></i> Pulchowk, Jhamshikhel Road</small>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>