<?php /* Template Name: Democracy Page */ ?>

<?php get_header(); ?>

<div class="row">
  <?php 
        // get the most recent activist
        $args = array(
                 'taxonomy' => 'activist',
                 'orderby' => 'date',
                 'order'   => 'ASC'
             );

        $cats = get_categories($args);
        //print_r($cats);
        // $category=($cats[0]->term_id);
        foreach ($cats as $cat):
          $category=($cat->term_id);
  ?>
  <div class="col-lg-6">
    <div class="card" style="width: 18rem;">
      <?php

          //get the posts in ascending order
          $posts =new WP_Query(
            array( 
              'post_type' => 'weeks',
              'orderby' => 'date',
              'number_of_posts'=>1,
              'order'   => 'ASC',
              'tax_query' => array(
                array(
                    'taxonomy' => 'activist',
                    'field' => 'term_id',
                    'terms' => $category,
                )
              )
            )
          );
          if($posts->have_posts()): 
                while($posts->have_posts()): $posts->the_post();
        ?>

      <a href="<?php echo get_the_permalink(); ?>">
        <img src="<?php the_field( 'image', 'activist_' . $category );?>" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title"><?php the_field('caption_1','activist_' . $category); ?></h5>
          <p class="card-text"><?php the_field('caption_2','activist_' . $category); ?></p>
        </div>
      </a>
    <?php endwhile;endif;wp_reset_query();?>
    </div>
  </div>

<?php endforeach; ?>
</div>



<?php get_footer(); ?>